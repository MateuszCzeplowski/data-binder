## Requirements

You need npm and http-server installed on your device.

## Run

1. Open terminal in project catalog and run command `npm i`.
2. Run command `npm run compile`.
3. Go to catalog dist in project and run `http-server`.

Default link: `http://localhost:8081/`

## Run dev

1. Open terminal in project catalog and run command `npm i`.
2. Run command `npm run dev`.

Link: `http://localhost:8080/`