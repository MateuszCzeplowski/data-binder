import DataBinder from './dataBinder.js';
const dataBinder = new DataBinder();

let model = {
  label: 'Mateusz Czepłowski',
  x: {
    y: {
      z: 'title from model'
    }
  }
};

const template = '<h1 data-bind="label" data-bind-title="x.y.z" data-bind-src="x.b"></h1>';

dataBinder.prepareTemplate(template);
model = dataBinder.bindModel(model);

setTimeout(() => {
  console.log('Setting title');
  model.x.y.z = 'updated title';
}, 1000);

setTimeout(() => {
  console.log('Setting empty content');
  model.label = '';
}, 2000);

setTimeout(() => {
  console.log('Setting content');
  model.label = 'Schibsted Tech Polska';
}, 3000);
