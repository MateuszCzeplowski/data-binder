export default class DataBinder {
  constructor() {
    this.attributes = ['data-bind', 'data-bind-title', 'data-bind-href', 'data-bind-alt', 'data-bind-src'];
  }

  getAttributes() {
    return this.attributes;
  }

  getElement() {
    return this.element;
  }

  setElement(element) {
    this.element = element;
  }

  prepareTemplate(template) {
    const element = this.createDOMElement(template);
    this.setElement(element);
  }

  bindModel(model) {
    const handler = {
      set: (object, name, value) => {
        if (typeof value === 'object') {
          this.deepProxy(value, handler);
          object[name] = new Proxy(value, handler);
        } else {
          object[name] = value;
        }
        this.insertData(model);
        return true;
      }
    };

    let proxy = new Proxy({}, handler);
    proxy.model = model;
    return proxy.model;
  }

  getAttributesToInsert() {
    return this.getAttributes()
      .map((attribute) => {
        const data = this.getElement().getAttribute(attribute);
        if (data) {
          const name = attribute.replace(/data-bind-?/, '');
          return { data, name };
        }
      })
      .filter(e => e);
  }

  insertData(model) {
    this.getAttributesToInsert()
      .forEach(({ data, name }) => {
        const attrData = this.getDataFromObject(data, model);

        if (!attrData && attrData !== "") {
          return;
        }

        if (!name.length) {
          return this.getElement().innerHTML = attrData;
        }

        return this.getElement().setAttribute(name, attrData);
      });

    return document.body.appendChild(this.getElement());
  }

  createDOMElement(template) {
    const container = document.createElement('div');
    container.innerHTML = template.trim();
    return container.firstChild;
  }

  getDataFromObject(data, model) {
    const objectArray = data.split('.');
    let currentValue = model;
    for (let i = 0; i < objectArray.length; i += 1) {
      if (currentValue[objectArray[i]] === undefined) {
        return;
      }
      currentValue = currentValue[objectArray[i]];
    }

    return currentValue;
  }

  deepProxy(data, handler) {
    const keys = Object.keys(data);
    const values = Object.values(data);
    values.forEach((nestedValue, i) => {
      if (typeof nestedValue === 'object') {
        data[keys[i]] = new Proxy({ ...nestedValue }, handler);
        this.deepProxy(data[keys[i]], handler);
      }
    });
  }
}
